# ML-Container

This is an exemplary container for machine learning purposes on GPUs

to run this image with singularity run the command for GPU optimised container
```
singularity exec --nv docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-gpu/ml-gpu:latest bash
```

If you want to extend the image you can simply fork this repository and add additional packages in the [`Dockerfile`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-images/ml-gpu/blob/master/Dockerfile) and possibly create a merge request.
with the flag `-B` you can add other directories to the container of needed.